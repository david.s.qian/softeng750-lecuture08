import { listTodosThunk, createTodoThunk, updateTodoThunk, deleteTodoThunk } from '../actions';
import moment from 'moment';

/**
 * Applies the given action to the given todo-list state, if applicable.
 * 
 * @param state the current list of todo items in the store
 * @param action the action to apply to the store
 */
export default function (state = [], action) {
    switch (action.type) {
        case listTodosThunk.actionTypes.success:
            return listTodosSuccess(state, action.payload);
        case updateTodoThunk.actionTypes.success:
            return updateTodosSuccess(state, action.payload);
        case deleteTodoThunk.actionTypes.success:
            return deleteTodosSuccess(state, action.payload);

        default:
            return state;
    }
}

/**
 * Returns a new todo list created by merging the old todo list and the one contained within the given payload.
 * 
 * The merged list will contain all todos from both lists, except in the case where more than one todo has the same id.
 * In that case, the one with the most recent "modified" date will be kept, and the other discarded.
 * 
 * @param oldState the old todo list
 * @param payload  the todo list to merge with the old one
 */
function listTodosSuccess(oldState, payload) {
    // Create a new array, where, foreach todo in the existing array...
    const mergedTodos = oldState.map((todo, index) => {

        // If an incoming todo has the same id as the todo at this index, AND that
        // incoming todo has a later modified time...
        const match = payload.find(inc => inc._id === todo._id);
        if (match && moment(match.modified).isAfter(todo.modified)) {
            // substitute the incoming todo
            return match;
        }

        // Otherwise, keep the current todo
        return todo;
    });

    // Now, add all the incoming todos which have different ids
    return mergedTodos.concat(payload.filter(todoA => oldState.find(todoB => todoA._id === todoB._id) === undefined));
}

function updateTodosSuccess(oldState, payload) {
    // Find the specific todo item by the id
    const targetItemId = payload._id;
    const targetIndex = oldState.findIndex(item => targetItemId === item._id);

    // Copy the origin state
    const updatedState = { ...oldState[targetIndex], ...payload };

    // return the final result by merging the objects
    const updatedTodoItems = [
        ...oldState.slice(0, targetIndex),
        updatedState,
        ...oldState.slice(targetIndex + 1),
    ];

    return updatedTodoItems;
}

function deleteTodosSuccess(oldState, payload) {
    return oldState.filter((todo) => {
        return todo._id !== payload
    });
}
